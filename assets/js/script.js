$(document).ready(function() {
	$('#upload-post').click(function(event) {

		var url = 'posts/add'; //url that will process the data.

		//create a new form data object to append values to send via post
		var formData = new FormData();
		if($('#title').val() == '' || typeof $('#image')[0].files[0] == 'undefined' ) {
			alert('Title and file are required');
		} 
		else {


		//append title and image values
		formData.append('title', $('#title').val());
		formData.append('image', $('#image')[0].files[0]);
		
		
		$.ajax({
			url : url,
			type : 'POST',
			data : formData,
			processData: false,
			contentType: false,
			success : function(data) {

				//if there was an error make a case of the different possible errors.
				if(data.status == 'error') {
					switch(data.errorType) {
						case 'filetype' : 
							alert('File type not allowed. Only GIF, JPG and PNG are accepted');
							break;
						case 'imagesize' :
							alert('Image size exceeded. Maximum dimensions are 1920x1080.');
							break;
						case 'filesize' : 
							alert('File size exceeded. Maximum file size is 2MB');
							break;
						default:
							alert('Unexpected error');
							break;
					}
					//else is when the upload was successful
				} else {
					//append the new post HTML code
					$('#posts-holder').html(data.element + $('#posts-holder').html());
					//change posts number
					var posts = parseInt($('#posts-count').html()) + 1;
					$('#posts-count').html(posts);
					//clean the form inputs
					$('#title').val('');
					$('#image').val('');
				}
			}
			
		});
		}
	});
});