<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index() {
		$this->load->model('PostsModel');
		$this->load->model('ViewsModel');

		$posts = $this->PostsModel->get_all_posts();
		$data['posts'] = $posts;
		$data['views'] = $this->ViewsModel->get_count_views();

		$this->load->view('home', $data);

		$this->ViewsModel->add_view();
		
	}
}
