<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {
	private $allowed_extensions = array('gif','png','jpg','jpeg');
	private $max_width = 1920;
	private $max_height = 1080;
	private $max_filesize = 2000000;

	public function __construct() {
		parent::__construct();
		$this->load->model('PostsModel');
	}

	public function index() {
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');

		$file = fopen('php://output', 'w');

		$posts = $this->PostsModel->get_all_posts();
		
		//$file = fopen(getcwd().'/assets/posts/' . $csv_file_name, 'w+');

		foreach ($posts as $post) {
			$row = array($post->title, $post->file);
			fputcsv($file, $row);
		}

	}

	public function add() {
		//we're returning a json object as a response
		header('Content-type: application/json');

		//catch post inputs
		$title = $this->input->post('title');
		$image = $_FILES['image'];

		//get file extension to validate
		$extension = pathinfo($image['name'], PATHINFO_EXTENSION);
		
		$response = array('status');

		if(!in_array($extension,$this->allowed_extensions)) {
			$response['status'] = 'error';
			$response['errorType'] = 'filetype';

			echo json_encode($response);
			exit(0);
		}

		$image_size = getimagesize($image['tmp_name']);
		
		//array in pos 0 = width, array in pos 1 = height
		if($image_size[0] > $this->max_width || $image_size[1] > $this->max_height) {
			$response['status'] = 'error';
			$response['errorType'] = 'imagesize';

			echo json_encode($response);
			exit(0);
		}
		
		//if image size exceeds the maximum permitted
		if(intval($image['size']) > $this->max_filesize) {
			$response['status'] = 'error';
			$response['errorType'] = 'filesize';

			echo json_encode($response);
			exit(0);
		}
		
		//create a new filename based on the timestamp
		$file_name = time() . '.' . $extension;
		if(move_uploaded_file($image['tmp_name'], getcwd(). "/assets/posts/{$file_name}")) {
			$this->PostsModel->add_post($title, $file_name);

			$response['status'] = 'success';
			$response['file'] = $file_name;
			$response['title'] = $title;
			//return the element for the new post so we can append it on HTML without reloading the page
			$response['element'] = '<div class="img-section">
		<div class="row">
			<div class="col-lg-4 col-sm-3 col-xs-1"></div>
			<div class="col-lg-4 col-sm-6 col-xs-10">
				<div>
					<div class="caption">
						<h4>'. $title .'</p>
					</div>
					<img class="img-responsive" src="assets/posts/' . $file_name .'">
				</div>
			</div>
			<div class="col-lg-4 col-sm-3 col-xs-1"></div>
		</div>
	</div>';

			echo json_encode($response);
		}

	}	
}