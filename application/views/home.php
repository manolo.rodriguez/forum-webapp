<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Manolo's test</title>
	<meta name="description" content="Test forum webapp">
	<meta name="author" content="Manolo Rodriguez">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<script src="assets/js/script.js"></script>
</head>

<body>
	<div class="top-bar container">
		<div class="row">
			<div class="col-xs-4 text-center">
				<p class="lead">Posts: <strong id="posts-count"><?php echo count($posts)?></strong></p>
			</div>
			<div class="col-xs-4 text-center">
				<a class="btn btn-primary btn-block" href="posts" target="_blank" role="button">Export</a>
			</div>
			<div class="col-xs-4 text-center">
				<p class="lead">Views: <strong><?php echo $views?></strong></p>
			</div>
		</div>
	</div>

	<div class="reply-box container">
		<div class="row">
			<div class="col-md-3 col-xs-1"></div>
			<div class="col-md-6 col-xs-10">
				<form>
					<div class="form-group">
						<label for="title">Image title</label>
						<input type="text" class="form-control" id="title" placeholder="Title">
					</div>
					<div class="form-group">
						<label for="image">Select Image</label>
						<input type="file" id="image">
					</div>

					<button id="upload-post" type="button" class="btn btn-default">Submit</button>
				</form>
			</div>
			<div class="col-md-3 col-xs-1"></div>
		</div>
		
	</div>
	<div id="posts-holder">
	<?php foreach($posts as $post):?>
	<div class="img-section">
		<div class="row">
			<div class="col-lg-4 col-sm-3 col-xs-1"></div>
			<div class="col-lg-4 col-sm-6 col-xs-10">
				<div>
					<div class="caption">
						<h4><?php echo $post->title?></p>
					</div>
					<img class="img-responsive" src="assets/posts/<?php echo $post->file?>">
				</div>
			</div>
			<div class="col-lg-4 col-sm-3 col-xs-1"></div>
		</div>
	</div>
	<?php endforeach;?>
	</div>
</body>
</html>