<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewsModel extends CI_Model {

	public function __construct() {
		parent::__construct();

	}
	public function add_view() {

		$post = array('id_view'=>'NULL');
		
		$this->db->insert('views',$post);

	}

	public function get_count_views() {
		
		$query = $this->db->query("SELECT COUNT(*) as count FROM forum_views");

		return $query->result()[0]->count;
	}
}