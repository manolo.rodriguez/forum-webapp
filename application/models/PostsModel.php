<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostsModel extends CI_Model {

	public function __construct() {
		parent::__construct();

	}
	public function add_post($title, $file) {

		$post = array('title'=>$title, 'file'=>$file);
		
		$this->db->insert('posts',$post);

	}

	public function get_all_posts() {
		
		$this->db->from('posts');
		$this->db->order_by('date', 'desc');
		$query = $this->db->get(); 

		return $query->result();
	}
}